## Configuration JSONs

### The config_data folder contains the metadata and workflow JSONs for all the supported activities and garments.


#### Each branch is referred to a specific environment
+ master: configurations for staging env (released by PynocchioPlatformStaging codepipeline)
+ develop: configurations for dev env (released by PynocchioPlatformDevelopment codepipeline)
+ test_pipeline: configurations for testing (released by PynocchioPlatformTest codepipeline)
